// SPDX-License-Identifier: 0BSD OR MIT-0

#include <assert.h>
#include <stdint.h>
#include <string.h>
/* getentropy() might be declared in either */
#include <sys/random.h>
#include <unistd.h>

#include "pcg.h"

/**/
